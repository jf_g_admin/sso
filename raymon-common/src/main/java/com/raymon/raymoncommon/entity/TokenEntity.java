package com.raymon.raymoncommon.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * @author JianFeiGan
 * @date 2022/4/22
 * @description token实体
 */
@Data
public class TokenEntity implements Serializable {

    private String accessToken;
    private String tokenType;
    private String refreshToken;
    private int expiresIn;
    private Set<String> scope;
    private Map<String, Object> additionalInformation;
}
