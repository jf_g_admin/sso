package com.raymon.raymoncommon.result;

import lombok.Getter;
import lombok.ToString;

/**
 * @author helen
 * @since 2019/12/25
 */
@Getter
@ToString
public enum ResultCodeEnum {


    NULL_POINT_EREXCEPTION(false, 00000, "空指针异常，数据有问题"),

    SUCCESS(true, 20000, "操作成功"),
    BUSINESS_ERROR(false, 29000, null),
    UNKNOWN_REASON(false, 20001, "操作失败"),
    UNABLE_REASON(false, 20002, "未能处理"),
    INVALID_URL(false, 20004, "无效接口"),
    EXCEPTION_REASON(false, 20005, "未知错误"),
    DATA_NOT_EXIST(false, 20006, "数据不存在"),
    DATA_EXIST(false, 20007, "数据已存在"),
    OVERDUE_CANNOT_OPERATE(false, 20008, "已过期，不可再操作"),
    TIMEOUT_CANNOT_OPERATE(false, 20009, "已超时，不可再操作"),
    FINISHED_CANNOT_OPERATE(false, 20010, "已完成，不可再操作"),

    BAD_SQL_GRAMMAR(false, 21001, "sql语法错误"),
    JSON_PARSE_ERROR(false, 21002, "json解析异常"),
    PARAM_ERROR(false, 21003, "参数不正确"),
    BAD_SQL_DATA_INTEGRITY_VIOLATION(false, 21004, "数据不合法"),
    INCOMPLETE_PARAMETERS(false, 21005, "参数不齐全"),

    FILE_UPLOAD_ERROR(false, 21004, "文件上传错误"),
    FILE_DELETE_ERROR(false, 21005, "文件刪除错误"),
    EXCEL_DATA_IMPORT_ERROR(false, 21006, "Excel数据导入错误"),

    VIDEO_UPLOAD_ALIYUN_ERROR(false, 22001, "视频上传至阿里云失败"),
    VIDEO_UPLOAD_TOMCAT_ERROR(false, 22002, "视频上传至业务服务器失败"),
    VIDEO_DELETE_ALIYUN_ERROR(false, 22003, "阿里云视频文件删除失败"),
    FETCH_VIDEO_UPLOADAUTH_ERROR(false, 22004, "获取上传地址和凭证失败"),
    REFRESH_VIDEO_UPLOADAUTH_ERROR(false, 22005, "刷新上传地址和凭证失败"),
    FETCH_PLAYAUTH_ERROR(false, 22006, "获取播放凭证失败"),
    SAVE_VIDEO_SCHEDULE_ERROR(false, 22007, "课程视频进度正在保存中，请稍后重试"),

    URL_ENCODE_ERROR(false, 23001, "URL编码失败"),
    ILLEGAL_CALLBACK_REQUEST_ERROR(false, 23002, "非法回调请求"),
    FETCH_ACCESSTOKEN_FAILD(false, 23003, "获取accessToken失败"),
    FETCH_USERINFO_ERROR(false, 23004, "获取用户信息失败"),
    LOGIN_ERROR(false, 23005, "登录失败"),
    TOKEN_VERIFICATION_FAIL(false, 23006, "令牌校验失败"),
    LOGIN_LOG_ERROR(false, 23007, "登录日志记录失败"),

    COMMENT_EMPTY(false, 24006, "评论内容必须填写"),

    PAY_RUN(false, 25000, "支付中"),
    PAY_UNIFIEDORDER_ERROR(false, 25001, "统一下单错误"),
    PAY_ORDERQUERY_ERROR(false, 25002, "查询支付结果错误"),

    ORDER_EXIST_ERROR(false, 25003, "课程已购买"),

    GATEWAY_ERROR(false, 26000, "服务不能访问"),
    FEIGN_TIMEOUT(false, 26001, "远程调用超时"),
    NETWORKERROR(false, 26002, "网络不通畅请稍后重试"),

    CODE_ERROR(false, 28000, "验证码错误"),

    MSG_SEND_ERROR(false, 28009, "短信发送失败"),
    LOGIN_PHONE_ERROR(false, 28009, "手机号码不正确"),
    LOGIN_USERNAME_ERROR(false, 28001, "账号不正确"),
    LOGIN_PASSWORD_ERROR(false, 28008, "密码不正确"),
    LOGIN_DISABLED_ERROR(false, 28002, "该用户已被禁用"),
    REGISTER_MOBLE_ERROR(false, 28003, "手机号已被注册"),
    LOGIN_AUTH(false, 28004, "需要登录"),
    LOGIN_ACL(false, 28005, "没有权限"),
    SMS_SEND_ERROR(false, 28006, "短信发送失败"),
    SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL(false, 28007, "短信发送过于频繁"),
    USER_NO_AUTHORITY(false, 28008, "用户没有任何权限"),
    LOGIN_METHOD_NOT_SUPPORT(false, 28009, "登录方式不支持"),
    OPEN_PLATFORM_NULL(false, 28010, "微信开放平台参数未设置"),
    OPEN_WEB_APPLICATION_NULL(false, 28011, "微信开放平台网站应用参数未设置"),
    OBTAIN_PERMISSION_FAILED(false, 28012, "获取权限信息失败"),
    ACCOUNT_CANNOT_LOGIN_CURRENT_PLATFORM(false, 28013, "账号无法登录当前平台"),
    ACCOUNT_OR_PASSWORD_WRONG(false, 28014, "账号或密码错误"),
    OPEN_WEB_APPLICATION_APP_ID_NULL(false, 28021, "微信开放平台网站应用AppId未设置"),
    OPEN_WEB_APPLICATION_APP_SECRET_NULL(false, 28022, "微信开放平台网站应用AppSecret未设置"),
    OPEN_WEB_APPLICATION_REDIRECT_URI_NULL(false, 28022, "微信开放平台网站应用RedirectUri未设置"),
    OPEN_WEB_APPLICATION_LOGIN_URL_NULL(false, 28023, "微信开放平台网站应用LoginUrl未设置"),
    WECHAT_ACCOUNT_NOT_BIND(false, 28109, "微信账号未绑定已有系统帐号"),
    PUBLIC_PLATFORM_IS_NULL(false, 28201, "公众号相关参数未配置"),
    USER_INFO_NULL_ERROR(false, 28301, "用户信息为空"),
    STUDENT_NOT_ENTRANCE(false, 28302, "暂无教学计划,请入学后再登录"),
    MY_VALUE(false, 30000, "我的错误"),
    AUTHORIZE_CODE_WRONG(false, 30001, "授权码错误"),
    WECHAT_BIND_ACCOUNT_ERROR(false, 30002, "账号绑定微信错误"),
    WECHAT_ALREADY_BIND(false, 30003, "微信账号已经被绑定"),
    ACCOUNT_BAN(false, 30004, "账号封禁中"),
    INVALID_DATA(false, 30005, "无效数据"),
    USER_STATUS_STOP(false, 30006, "账号已被停用，无法登陆"),

    CLIENT_ACCESS_DENIED(false, 40001, "令牌过期或权限不足,请重新登录"),
    USER_ACCESS_DENIED(false, 40002, "用户权限不足,无法访问"),
    SECRET_KEY_MISSING(false, 41001, "密钥不能空,请指定加密密钥"),
    RESOURCE_ID_MISSING(false, 41002, "资源服务器ID未指定"),
    TOKEN_EXPIRE_MISSING(false, 41003, "access-token或者refresh-token有效时间未指定"),
    PUBLIC_KEY_MISSING(false, 41004, "public-key公钥未指定"),
    PUBLIC_KEY_GENERATE_FAIL(false, 41005, "生成公钥失败"),
    PROCESSING_URL_MISSING(false, 41006, "登录处理url未指定"),
    TOKEN_SHOULD_NOT_BE_TRUSTED(false, 41007, "令牌不可信");

    private Boolean success;

    private Integer code;

    private String message;

    ResultCodeEnum(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }
}
