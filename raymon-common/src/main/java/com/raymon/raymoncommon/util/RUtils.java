package com.raymon.raymoncommon.util;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.raymon.raymoncommon.result.R;
import lombok.Data;


import java.util.List;
import java.util.stream.Collectors;

/**
 * 处理R返回值的工具类
 */
@Data
public class RUtils {

    /**
     * 获取item
     *
     * @param r        统一返回结果
     * @param classObj 返回数据类型
     * @param <T>
     * @return
     */
    public static <T> T getItem(R r, Class<T> classObj) {
        //得到数据内容
        Object item = r.getData().get("item");
        //将map的数据先装JSON字符串，然后在转Java对象
        T t = JSONObject.parseObject(JSONObject.toJSONString(item, SerializerFeature.WriteNullListAsEmpty,SerializerFeature.WriteNullBooleanAsFalse,
                SerializerFeature.WriteNullStringAsEmpty), classObj);
        return t;

    }

    /**
     * @param r        统一返回结果
     * @param key      R的data的key
     * @param classObj 返回数据类型
     * @param <T>
     * @return
     */
    public static <T> T getItem(R r, String key, Class<T> classObj) {
        //得到数据内容
        Object item = r.getData().get(key);
        //将map的数据先装JSON字符串，然后在转Java对象
        T t = JSONObject.parseObject(JSONObject.toJSONString(item,SerializerFeature.WriteNullListAsEmpty,SerializerFeature.WriteNullBooleanAsFalse,
                SerializerFeature.WriteNullStringAsEmpty), classObj);
        return t;

    }

    /**
     * 获取items
     *
     * @param r        统一返回结果
     * @param classObj 返回数据类型
     * @param <T>
     * @return
     */
    public static <T> List<T> getItems(R r, Class<T> classObj) {
        //得到数据内容
        Object items = r.getData().get("items");
        //将map的数据先装JSON字符串，然后在转Java对象
        List<T> parseArray = JSONArray.parseArray(JSONArray.toJSONString(items,SerializerFeature.WriteNullListAsEmpty,SerializerFeature.WriteNullBooleanAsFalse), classObj);
        return parseArray;
    }

    /**
     * @param r        统一返回结果
     * @param key      R的data的key
     * @param classObj 返回数据类型
     * @param <T>
     * @return
     */
    public static <T> List<T> getItems(R r, String key, Class<T> classObj) {
        //得到数据内容
        Object items = r.getData().get(key);
        //将map的数据先装JSON字符串，然后在转Java对象
        List<T> parseArray = JSONArray.parseArray(JSONArray.toJSONString(items,SerializerFeature.WriteNullListAsEmpty,SerializerFeature.WriteNullBooleanAsFalse), classObj);
        return parseArray;
    }

    /**
     * 获取items
     *
     * @param r
     * @param classObj
     * @param <T>
     * @return
     */
    public static <T> List<T> getrows(R r, Class<T> classObj) {
        //得到数据内容
        Object rows = r.getData().get("rows");
        //将map的数据先装JSON字符串，然后在转Java对象
        List<T> parseArray = JSONArray.parseArray(JSONArray.toJSONString(rows,SerializerFeature.WriteNullListAsEmpty,SerializerFeature.WriteNullBooleanAsFalse), classObj);
        return parseArray;
    }

    public static <T> Page<T> getPage(R result, Class<T> clazz) {
        Object item = result.getData().get("item");
        Gson gson = new Gson();
        Page<T> pageRecord = gson.fromJson(gson.toJson(item), new TypeToken<Page<T>>() {
        }.getType());

        List<T> list = pageRecord.getRecords().stream().map(data -> {
            T t = gson.fromJson(gson.toJson(data), clazz);
            return t;
        }).collect(Collectors.toList());

        pageRecord.setRecords(list);

        return pageRecord;
    }
}
