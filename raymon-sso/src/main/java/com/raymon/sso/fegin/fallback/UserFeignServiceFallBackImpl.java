package com.raymon.sso.fegin.fallback;


import com.raymon.raymoncommon.result.R;
import com.raymon.raymoncommon.result.ResultCodeEnum;
import com.raymon.sso.fegin.UserFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 错误回调
 */
@Service
@Slf4j
public class UserFeignServiceFallBackImpl implements UserFeignService {

    @Override
    public R loginUser(String userName) {
        log.info("调用根/sys/user/login/{userName}，熔断保护");
        return R.error(ResultCodeEnum.FEIGN_TIMEOUT).data("details", "调用/sys/user/login/{userName}");
    }

    @Override
    public R loginUserPermission(String id) {
        log.info("调用根/sys/user/login/permission/{id}，熔断保护");
        return R.error(ResultCodeEnum.FEIGN_TIMEOUT).data("details", "调用/sys/user/login/permission/{id}");
    }
}
