package com.raymon.sso.fegin;


import com.raymon.raymoncommon.result.R;
import com.raymon.sso.fegin.fallback.UserFeignServiceFallBackImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 用户远程调用服务类
 */
@FeignClient(value = "raymonSys", fallback = UserFeignServiceFallBackImpl.class)
public interface UserFeignService {

    @GetMapping("/sys/user/login/{userName}")
    public R loginUser(@PathVariable("userName") String userName);


    @GetMapping("/sys/user/login/permission/{id}")
    public R loginUserPermission(@PathVariable("id") String id);
}
