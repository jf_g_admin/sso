package com.raymon.sso.handler;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 登录失败处理器
 */
public class SSOAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        System.out.println("登录失败处理器");

        Map<String, Object> map = new HashMap<>();
        map.put("state", 500);
        map.put("message", "登录失败  登录失败处理器");
        //将map对象转换为json格式字符串并写到客户端
        writeJsonToClient(httpServletResponse, map);
    }

    /*提取共性代码*/
    private void writeJsonToClient(
            HttpServletResponse response,
            Map<String, Object> map) throws IOException {
        //将map对象,转换为json
        String json = new ObjectMapper().writeValueAsString(map);
        //设置响应数据的编码方式
        response.setCharacterEncoding("utf-8");
        //设置响应数据的类型
        response.setContentType("application/json;charset=utf-8");
        //将数据响应到客户端
        PrintWriter out = response.getWriter();
        out.println(json);
        out.flush();
    }
}
