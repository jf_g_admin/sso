package com.raymon.sso.handler;

import cn.hutool.json.JSONUtil;
import com.raymon.raymoncommon.entity.TokenEntity;
import com.raymon.sso.activemq.ActivemqSendMsgService;
import com.raymon.sso.config.AuthorizationServerOauth2Config;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 登录成功处理器
 */
public class SSOAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Autowired
    @Qualifier("tokenServices")
    private AuthorizationServerTokenServices tokenServices;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ActivemqSendMsgService activemqSendMsgService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        User user = (User) authentication.getPrincipal();
        System.out.println("------------登录成功----------");
        System.out.println(user.getUsername());
        System.out.println(user.getPassword());
        System.out.println(user.getAuthorities());
        Map<String, Object> map = new HashMap<>();
        map.put("state", 200);
        map.put("message", "login ok");
        //将map对象转换为json格式字符串并写到客户端
        String clientId = "raymon-sys";
        ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);
        TokenRequest tokenRequest = new TokenRequest(null, clientId, clientDetails.getScope(), "password");
        OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);
        OAuth2AccessToken accessToken = tokenServices.getAccessToken(new OAuth2Authentication(oAuth2Request, authentication));
        /*重新使用刷新令牌获取token*/
//        //使用密码模式
//        TokenRequest tokenRequest = new TokenRequest(null, clientId, clientDetails.getScope(), "password");
//        OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);
//        OAuth2AccessToken accessToken = tokenServices.createAccessToken(new OAuth2Authentication(oAuth2Request, authentication));

        /*暂时将username作为 redis key*/
        saveRedis(accessToken, user.getUsername());
        /*登录发送MQ*/
        sendMQ(user);

        writeJsonToClient(httpServletResponse, map);
    }
    private void saveRedis(OAuth2AccessToken accessToken, String id) {
        Map<String, Object> map = accessToken.getAdditionalInformation();
        String jti = (String) map.get("jti");
        String key = "raymon:token" + id + ":" + jti;
        OAuth2RefreshToken refreshToken = accessToken.getRefreshToken();

        TokenEntity tokenEntity = new TokenEntity();
        String jwtToken = accessToken.getValue();
        List<String> jwtTokenList = Arrays.stream(jwtToken.split("\\.")).collect(Collectors.toList());
        jwtTokenList.remove(2);
        String newJwtToken = jwtTokenList.stream().collect(Collectors.joining("."));
        tokenEntity.setAccessToken(newJwtToken);
        tokenEntity.setExpiresIn(accessToken.getExpiresIn());
        tokenEntity.setTokenType(accessToken.getTokenType());
        tokenEntity.setRefreshToken(refreshToken.getValue());
        tokenEntity.setScope(accessToken.getScope());
        tokenEntity.setAdditionalInformation(accessToken.getAdditionalInformation());

        int accessTokenExpire = accessToken.getExpiresIn();
        redisTemplate.opsForValue().set(key, tokenEntity, accessTokenExpire, TimeUnit.SECONDS);
    }

    private void sendMQ(User user) {
        String queueName = "LoginMQMsg";
        String s = JSONUtil.toJsonStr(user);
        activemqSendMsgService.sendMessage(queueName,s);
    }

    /*提取共性代码*/
    private void writeJsonToClient(
            HttpServletResponse response,
            Map<String, Object> map) throws IOException {
        //将map对象,转换为json
        String json = new ObjectMapper().writeValueAsString(map);
        //设置响应数据的编码方式
        response.setCharacterEncoding("utf-8");
        //设置响应数据的类型
        response.setContentType("application/json;charset=utf-8");
        //将数据响应到客户端
        PrintWriter out = response.getWriter();
        out.println(json);
        out.flush();
    }
}
