package com.raymon.sso.service.impl;

import com.raymon.raymoncommon.result.R;
import com.raymon.raymoncommon.util.RUtils;
import com.raymon.raymoncommon.vo.LoginVo;
import com.raymon.sso.fegin.UserFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description
 */
@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserFeignService userFeignService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 基于用户名获取数据库中的用户信息
     * @param username 这个username来自系统服务
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {


        //基于feign方式获取远程数据并封装
        //1.基于用户名获取用户信息
        R r = userFeignService.loginUser(username);
        LoginVo loginVo = RUtils.getItem(r, LoginVo.class);
        if (loginVo == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        //2.基于用于id查询用户权限
        R r1 = userFeignService.loginUserPermission(String.valueOf(loginVo.getId()));
        List<String> permissions = RUtils.getItems(r1, String.class);
        log.info("permissions {}", permissions);
        String password = passwordEncoder.encode(loginVo.getPassword());
        //3.对查询结果进行封装并返回
        User userInfo = new User(username, password,
                AuthorityUtils.createAuthorityList(permissions.toArray(new String[]{})));
        //......
        return userInfo;
        //返回给认证中心,认证中心会基于用户输入的密码以及数据库的密码做一个比对
    }
}

