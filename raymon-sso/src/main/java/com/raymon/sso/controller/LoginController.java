package com.raymon.sso.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 登录接口
 */
@RestController
@RequestMapping("login")
public class LoginController {

    @RequestMapping("user")
    public String login() {
        return "登录成功";
    }
}
