package com.raymon.sso.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description redis配置
 */
@Configuration
public class RedisTokenConfig {

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    public TokenStore redisTokenStore() {
        return new RedisTokenStore(redisConnectionFactory);
    }
}
