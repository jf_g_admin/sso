package com.raymon.sso.config;

import com.raymon.sso.handler.SSOAuthenticationFailureHandler;
import com.raymon.sso.handler.SSOAuthenticationSuccessHandler;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description Security配置类
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * 初始化加密对象
     * 此对象提供了一种不可逆的加密方式,相对于md5方式会更加安全
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 定义认证管理器对象，这个对象负责完成用户信息的认证，
     * 即判定用户身份信息的合法性，在基于oauth2协议完成认
     * 证时，需要此对象，所以这里讲此对象拿出来交给spring管理
     */
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManager();
    }

    /*自定义登录逻辑*/
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
//    }

    /**
     * 配置认证规则
     */
    @Override
    protected void configure(HttpSecurity http)
            throws Exception {
        //super.configure(http);//默认所有请求都要认证
        //1.禁用跨域攻击(先这么写，不写的话在postman和idea中测试会报403异常)
//        http.csrf().disable();
        //2.放行所有资源的访问(后续可以基于选择对资源进行认证和放行)
//        http.authorizeRequests()
//
//                .anyRequest().permitAll();//所有都不需要认证
        http.authorizeRequests()
                /*放行 不需要验证*/
                .antMatchers("/oauth/**", "/login/**", "/logout/**")
                .permitAll()
//                .regexMatchers("") 正则过滤资源
//                .mvcMatchers("/login").servletPath("/**")
                /*所有请求必须认证才能访问*/
                .anyRequest()
                .authenticated()
                .and()
                .csrf()
                .disable();
       /* http.authorizeRequests()
        .anyRequest().authenticated();//所有需要认证*/
        /*除了default.html其余都得认证
        http.authorizeRequests().mvcMatchers("/default.html")
               .authenticated().anyRequest().permitAll();*/
        //3.设置认证结果处理器(默认认证成功会跳转到自己设置的index.html)自定义定义登录成功和失败以后的处理逻辑(可选)
        //假如没有如下设置登录成功会显示404
        http.formLogin()//方法执行后会创建一个/login路径
                .successHandler(new SSOAuthenticationSuccessHandler()).failureHandler(new SSOAuthenticationFailureHandler());
    }

}
