package com.raymon.sys.controller;

import com.raymon.raymoncommon.result.R;
import com.raymon.raymoncommon.vo.LoginVo;
import com.raymon.sys.entity.User;
import com.raymon.sys.service.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 用户控制层
 */
@RestController
@RequestMapping("/sys/user")
public class UserController {

    @Autowired
    private IUserService iUserService;

    @GetMapping("/{id}")
    public R getUserById(@PathVariable("id") String id) {
        User userById = iUserService.getUserById(Long.valueOf(id));
        return R.ok().data("item", userById);
    }

    @GetMapping("login/{userName}")
    public R loginUser(@PathVariable("userName") String userName) {
        User user = iUserService.loginUser(userName);
        LoginVo loginVo = new LoginVo();
        BeanUtils.copyProperties(user, loginVo);
        return R.ok().data("item", loginVo);
    }

    @GetMapping("login/permission/{id}")
    public R loginUserPermission(@PathVariable("id") String id) {
        List<String> permissions = iUserService.selectUserPermissions(id);
        return R.ok().data("items", permissions);
    }
}
