package com.raymon.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.raymon.sys.entity.User;
import com.raymon.sys.mapper.UserMapper;
import com.raymon.sys.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 用户服务实现类
 */
@Service
public class IUserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public User getUserById(Long id) {
        return this.getById(id);
    }

    @Override
    public User loginUser(String userName) {
        LambdaQueryWrapper<User> eq = new LambdaQueryWrapper<User>().eq(User::getName, userName);
        return this.getOne(eq);
    }

    @Override
    public List<String> selectUserPermissions(String id) {
        ArrayList<String> list = new ArrayList<>();
        list.add("权限菜单");
        return list;
    }
}
