package com.raymon.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.raymon.sys.entity.User;

import java.util.List;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 用户接口
 */
public interface IUserService extends IService<User> {

    public User getUserById(Long id);

    public User loginUser(String userName);

    public List<String> selectUserPermissions(String id);
}
