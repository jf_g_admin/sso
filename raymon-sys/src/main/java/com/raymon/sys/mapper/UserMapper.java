package com.raymon.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.raymon.sys.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description 用户
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
