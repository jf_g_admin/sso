package com.raymon.gateway.filter;

import com.alibaba.fastjson.JSONObject;
import com.raymon.raymoncommon.entity.TokenEntity;
import com.raymon.raymoncommon.result.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author JianFeiGan
 * @date 2022/4/21
 * @description
 */

@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {

    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        ServerHttpRequest request = exchange.getRequest();
        String path = request.getURI().getPath();
        System.out.println("==="+path);

        //登录和注册请求直接放行
        if(antPathMatcher.match("/login", path) ||
                antPathMatcher.match("/register", path)) {
            return chain.filter(exchange);
        }

        //api接口，异步请求，校验用户必须登录
        if(antPathMatcher.match("/api/**", path)) {
            String userName = this.getUserName(request);
            System.out.println("获取登录用户信息:"+userName);
            if(StringUtils.isEmpty(userName)) {
                ServerHttpResponse response = exchange.getResponse();
                return out(response, ResultCodeEnum.LOGIN_AUTH);
            }
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    /**
     * api接口鉴权失败返回数据
     * @param response
     * @return
     */
    private Mono<Void> out(ServerHttpResponse response, ResultCodeEnum resultCodeEnum) {
        byte[] bits = JSONObject.toJSONString("网关鉴权失败").getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        //指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }

    /**
     * 获取当前登录用户信息
     * @param request
     * @return
     */
    private String getUserName(ServerHttpRequest request) {
        String token = "";
        String name = "";
        String jti = "";
        List<String> tokenList = request.getHeaders().get("token");
        List<String> username = request.getHeaders().get("username");
        List<String> jtis = request.getHeaders().get("jti");
        if(null != tokenList) {
            token = tokenList.get(0);
        }
        if(null != username) {
            name = username.get(0);
        }
        if(null != jtis) {
            jti = jtis.get(0);
        }
        System.out.println("获取到的token:"+token);
        if(!StringUtils.isEmpty(token)) {
            // 通过获取到的token获取用户名并检查redis中是否保留登录信息
            // TODO
            // 如果redis中不存在用户登录信息则直接返回null
            String key = "raymon:token" + name + ":" + jti;
            TokenEntity tokenEntity = (TokenEntity) redisTemplate.opsForValue().get(key);
            return tokenEntity.getAccessToken();
        }
        return null;
    }

}
